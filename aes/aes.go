package aes

import (
	"crypto/cipher"
	"io"
)

func GenerateKey(size int, r io.Reader) ([]byte, error) {
	key := make([]byte, size)
	_, err := io.ReadFull(r, key[:])
	if err != nil {
		return nil, err
	}
	return key, nil
}

func GenerateNonce(size int, r io.Reader) ([]byte, error) {
	nonce := make([]byte, size)
	_, err := io.ReadFull(r, nonce[:])
	if err != nil {
		return nil, err
	}
	return nonce, nil
}

func Encrypt(gcm cipher.AEAD, encdek, nonce, message []byte) []byte {
	dst := append(encdek[:], nonce[:]...)
	out := gcm.Seal(dst, nonce, message, nil)
	return out
}

func Decrypt(gcm cipher.AEAD, nonce, message []byte) ([]byte, error) {

	out, err := gcm.Open(nil, nonce, message, nil)

	if err != nil {
		return nil, err
	}

	return out, nil
}
