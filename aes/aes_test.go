package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"testing"
)

var keyGenerateTests = []struct {
	length int
}{
	{0},
	{1},
	{32},
	{64},
	{128},
	{256},
	{512},
}

var dek = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^")
var encdek = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^")
var nonce = []byte("abcd01234567")
var message = []byte("Lorem Ipsum")

//Hex encoded result of encryption
var result = "4142434445464748494a4b4c4d4e4f505152535455565758595a21402324255e616263643031323334353637497522e7df99b62d6531e32c259779aff43ae14d20e11f8f0872d6"

func TestGenerateKey(t *testing.T) {

	for _, it := range keyGenerateTests {
		k, _ := GenerateKey(it.length, rand.Reader)

		if len(k) != it.length {
			t.Errorf("Key length was incorrect. Expected: %d, Received: %d", it.length, len(k))
		}
	}
}

func TestGenerateKeyError(t *testing.T) {

	_, err := GenerateKey(32, MockErrorRandReader{})

	if err == nil {
		t.Error("Should have received an error.")
	}
}

func TestGenerateNonce(t *testing.T) {
	n, _ := GenerateNonce(12, rand.Reader)
	if len(n) != 12 {
		t.Errorf("Incorrect nonce size. Expected: %d, Received %d", 12, len(n))
	}
}

func TestGenerateNonceError(t *testing.T) {
	_, err := GenerateNonce(12, MockErrorRandReader{})
	if err == nil {
		t.Error("Should have received an error.")
	}
}

func TestEncrypt(t *testing.T) {

	c, _ := aes.NewCipher(dek)
	gcm, _ := cipher.NewGCM(c)

	out := Encrypt(gcm, encdek, nonce, message)

	dst := make([]byte, hex.EncodedLen(len(out)))
	hex.Encode(dst, out)

	if string(dst) != result {
		t.Error("Encryption does not match expected result")
	}

	edk := make([]byte, len(dek))
	n := make([]byte, len(nonce))

	copy(edk, out[:len(dek)])
	copy(n, out[len(dek):len(dek)+len(nonce)])

	if string(edk) != string(encdek) {
		t.Errorf("Encrypted DEK does not match. Expected: %x, Received: %x", encdek, edk)
	}

	if string(n) != string(nonce) {
		t.Errorf("Nonce does not match. Expected: %x, Received %x", nonce, n)
	}

}

func TestDecrypt(t *testing.T) {
	c, _ := aes.NewCipher(dek)
	gcm, _ := cipher.NewGCM(c)
	msg, _ := hex.DecodeString(result)
	out, _ := Decrypt(gcm, nonce, msg[len(dek)+len(nonce):])

	if string(out) != string(message) {
		t.Errorf("Decrypted data does not match expected message. Expected: %x, Received: %x", message, out)
	}
}

func TestDecryptError(t *testing.T) {
	msg, _ := hex.DecodeString(result)
	_, err := Decrypt(MockErrorGCM{}, nonce, msg[len(dek)+len(nonce):])

	if err == nil {
		t.Error("Did not receive expected error.")
	}
}

//MockErrorRandReader returns an error on calls to Read
type MockErrorRandReader struct{}

func (m MockErrorRandReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("Lorem Ipsum Error")
}

//MockErrorGCM returns an error on calls to Open
type MockErrorGCM struct{}

func (m MockErrorGCM) NonceSize() int                                           { return 0 }
func (m MockErrorGCM) Overhead() int                                            { return 0 }
func (m MockErrorGCM) Seal(dst, nonce, plaintext, additionalData []byte) []byte { return []byte("") }
func (m MockErrorGCM) Open(dst, nonce, ciphertext, additionalData []byte) ([]byte, error) {
	return []byte(""), errors.New("Lorem Ipsum Error")
}
