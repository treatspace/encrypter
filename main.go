package main

import (
	"log"
	"os"

	"gitlab.com/treatspace/encrypter/file"

	"io"

	"bufio"

	"crypto/cipher"
	"crypto/rand"

	"crypto/aes"

	eaes "gitlab.com/treatspace/encrypter/aes"
	"gitlab.com/treatspace/encrypter/kms"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

const (
	KeySize    = 32
	NonceSize  = 12
	DEKKeySize = 113
)

var (
	app        = kingpin.New("encrypter", "A command-line encryption utility utilizing GCE KMS.")
	kmsProject = app.Flag("kms-project", "KMS Project name").PlaceHolder("gce-project-123").Envar("KMS_PROJECT").Required().String()
	kmsKeyring = app.Flag("kms-keyring", "KMS Keyring name").PlaceHolder("keyring").Envar("KMS_KEYRING").Required().String()
	kmsKey     = app.Flag("kms-key", "KMS Key name").PlaceHolder("key").Envar("KMS_KEY").Required().String()

	encrypt        = app.Command("encrypt", "Encrypt a plain-text file.")
	encryptInFile  = encrypt.Arg("in-file", "File to read from. Defaults to stdin").String()
	encryptOutFile = encrypt.Arg("out-file", "File to write to. Defaults to stdout").String()

	decrypt        = app.Command("decrypt", "Decrypt an encrypted file.")
	decryptInFile  = decrypt.Arg("in-file", "File to read from. Defaults to stdin").String()
	decryptOutFile = decrypt.Arg("out-file", "File to write to. Defaults to stdout").String()
)

func main() {
	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case encrypt.FullCommand():
		r, err := file.ReadBuffer(encryptInFile, os.Stdin)
		if err != nil {
			log.Fatal(err)
		}

		w, err := file.WriteBuffer(encryptOutFile, os.Stdout)
		if err != nil {
			log.Fatal(err)
		}
		defer w.Flush()

		out, err := encryptFile(r)
		if err != nil {
			log.Fatal(err)
		}

		w.Write(out)

	case decrypt.FullCommand():
		r, err := file.ReadBuffer(decryptInFile, os.Stdin)
		if err != nil {
			log.Fatal(err)
		}

		w, err := file.WriteBuffer(decryptOutFile, os.Stdout)
		if err != nil {
			log.Fatal(err)
		}
		defer w.Flush()

		out, err := decryptFile(r)
		if err != nil {
			log.Fatal(err)
		}

		w.Write(out)
	}
}

func encryptFile(r *bufio.Reader) ([]byte, error) {
	dek, err := eaes.GenerateKey(KeySize, rand.Reader)
	if err != nil {
		return nil, err
	}

	nonce, err := eaes.GenerateNonce(NonceSize, rand.Reader)
	if err != nil {
		return nil, err
	}

	encdek, err := kms.Encrypt(*kmsProject, *kmsKeyring, *kmsKey, dek)
	if err != nil {
		return nil, err
	}

	//Read file/stdin in 4MB chunks
	buf := make([]byte, 0, 4*1024*1024)
	msg := make([]byte, 0)
	for {
		n, err := r.Read(buf[:cap(buf)])
		buf = buf[:n]
		if n == 0 {
			if err == nil {
				continue
			}
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}

		msg = append(msg[:], buf[:]...)

		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
	}

	c, err := aes.NewCipher(dek)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	out := eaes.Encrypt(gcm, encdek, nonce, msg)

	return out, nil
}

func decryptFile(r *bufio.Reader) ([]byte, error) {
	//Read file/stdin in 4MB chunks
	buf := make([]byte, 0, 4*1024*1024)
	msg := make([]byte, 0)
	for {
		n, err := r.Read(buf[:cap(buf)])
		buf = buf[:n]
		if n == 0 {
			if err == nil {
				continue
			}
			if err == io.EOF {
				break
			}
			return nil, err
		}

		msg = append(msg[:], buf[:]...)

		if err != nil && err != io.EOF {
			return nil, err
		}
	}

	encdek := make([]byte, DEKKeySize)
	nonce := make([]byte, NonceSize)

	copy(encdek, msg[:DEKKeySize])
	copy(nonce, msg[DEKKeySize:DEKKeySize+NonceSize])

	dek, err := kms.Decrypt(*kmsProject, *kmsKeyring, *kmsKey, encdek)
	if err != nil {
		return nil, err
	}

	c, err := aes.NewCipher(dek)

	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)

	if err != nil {
		return nil, err
	}

	out, err := eaes.Decrypt(gcm, nonce, msg[DEKKeySize+NonceSize:])
	if err != nil {
		return nil, err
	}

	return out, nil
}
