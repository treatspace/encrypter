package file

import (
	"bufio"
	"errors"
	"fmt"
	"os"
)

func ReadBuffer(path *string, def *os.File) (*bufio.Reader, error) {

	if *path != "" {
		f, err := os.Open(*path)
		if os.IsNotExist(err) {
			return nil, errors.New(fmt.Sprintf("Unable to open file (%s) for reading", *path))
		}
		return bufio.NewReader(f), nil
	}
	return bufio.NewReader(def), nil
}

func WriteBuffer(path *string, def *os.File) (*bufio.Writer, error) {

	if *path != "" {
		f, err := os.Create(*path)
		if err != nil {
			return nil, err
		}
		return bufio.NewWriter(f), nil
	}
	return bufio.NewWriter(def), nil
}
