package file

import (
	"os"
	"testing"
)

func TestReadBuffer(t *testing.T) {

	path := "/does/not/exist"

	f, _ := ReadBuffer(&path, os.Stdin)
	if f != nil {
		t.Error("Should not be able to open buffer on non-existent file.")
	}

	path = ""

	f, _ = ReadBuffer(&path, os.Stdin)
	if f == nil {
		t.Error("Empty path should return buffer for os.Stdin")
	}

	path = "../tests/empty"

	f, _ = ReadBuffer(&path, os.Stdin)
	if f == nil {
		t.Error("Buffer should have been returned for ../tests/empty file")
	}
}

func TestWriteBuffer(t *testing.T) {
	path := "../tests/notexist"
	f, _ := WriteBuffer(&path, os.Stdout)
	if f == nil {
		t.Error("Non-existent file should have returned a new buffer")
	}
	os.Remove(path)

	path = "../tests/empty"
	f, _ = WriteBuffer(&path, os.Stdout)
	if f == nil {
		t.Error("Buffer should have been returned for existing file")
	}

	path = ""
	f, _ = WriteBuffer(&path, os.Stdout)
	if f == nil {
		t.Error("Buffer should have been returned for os.Stdout")
	}

	path = "../tests"
	_, e := WriteBuffer(&path, os.Stdout)
	if e == nil {
		t.Error("Writing to a directory should trigger an error")
	}

}
