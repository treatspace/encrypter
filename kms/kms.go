package kms

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"

	"golang.org/x/oauth2/google"
	cloudkms "google.golang.org/api/cloudkms/v1"
)

func Encrypt(projectID, keyRing, cryptoKey string, plainText []byte) ([]byte, error) {
	ctx := context.Background()
	client, err := google.DefaultClient(ctx, cloudkms.CloudPlatformScope)
	if err != nil {
		log.Fatal(err)
	}

	cloudkmsService, err := cloudkms.New(client)
	if err != nil {
		log.Fatal(err)
	}

	parentName := fmt.Sprintf("projects/%s/locations/%s/keyRings/%s/cryptoKeys/%s",
		projectID, "global", keyRing, cryptoKey)

	resp, err := cloudkmsService.Projects.Locations.KeyRings.CryptoKeys.
		Encrypt(parentName, &cloudkms.EncryptRequest{
			Plaintext: base64.StdEncoding.EncodeToString(plainText),
		}).Do()
	if err != nil {
		return nil, err
	}

	return base64.StdEncoding.DecodeString(resp.Ciphertext)
}

func Decrypt(projectID, keyRing, cryptoKey string, cipherText []byte) ([]byte, error) {
	ctx := context.Background()
	client, err := google.DefaultClient(ctx, cloudkms.CloudPlatformScope)
	if err != nil {
		log.Fatal(err)
	}

	cloudkmsService, err := cloudkms.New(client)
	if err != nil {
		log.Fatal(err)
	}

	parentName := fmt.Sprintf("projects/%s/locations/%s/keyRings/%s/cryptoKeys/%s",
		projectID, "global", keyRing, cryptoKey)

	resp, err := cloudkmsService.Projects.Locations.KeyRings.CryptoKeys.
		Decrypt(parentName, &cloudkms.DecryptRequest{
			Ciphertext: base64.StdEncoding.EncodeToString(cipherText),
		}).Do()
	if err != nil {
		return nil, err
	}
	return base64.StdEncoding.DecodeString(resp.Plaintext)
}
