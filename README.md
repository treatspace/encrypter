# Encrypter

[![build status](https://gitlab.com/treatspace/encrypter/badges/master/build.svg)](https://gitlab.com/treatspace/encrypter/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/treatspace/encrypter)](https://goreportcard.com/report/gitlab.com/treatspace/encrypter)

Encryption and decryption tool utilizing AES-GCM and GCE KMS

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Golang 1.8+

```
brew install go
```

Glide

```
curl https://glide.sh/get | sh
```

Gox
```
go get github.com/mitchellh/gox

```

### Installing

A step by step series of examples that tell you have to get a development env running

Checkout the source code

```
git clone git@gitlab.com:treatspace/encrypter.git
```

Install dependencies

```
glide install
```

Execute

```
go run main.go --help
```

End with an example of getting some data out of the system or using it for a little demo

### Usage


_Note: Flags can be supplied as environment variables KMS_PROJECT, KMS_KEYRING, KMS_KEY._

Encrypt using stdin and stdout. 

```
cat file.txt | encrypter --kms-project={project} --kms-keyring={keyring} --kms-key={key} encrypt > file.aes
```

Decrypt using stdin and stdout

```
cat file.aes | encrypter --kms-project={project} --kms-keyring={keyring} --kms-key={key} decrypt > file.txt
```

Encrypt using files

```
encrypter --kms-project={project} --kms-keyring={keyring} --kms-key={key} encrypt file.txt file.aes
```

Decrypt using files

```
encrypter --kms-project={project} --kms-keyring={keyring} --kms-key={key} decrypt file.aes file.txt
```

### Build

Gox is used to build the project for linux and osx. Executables will be stored in the cli directory.

```
gox -os="linux darwin" -arch="386 amd64" -output="./cli/{{.Dir}}_{{.OS}}_{{.Arch}}"
```
## Downloads

[Download](https://gitlab.com/treatspace/encrypter/tags) the version specific to your os and architecture.

## Built With

* [Golang](https://golang.org/) - Golang
* [Kingpin](https://github.com/alecthomas/kingpin) - The CLI Framework Used
* [Glide](https://glide.sh/) - The dependency management tool used
* [KMS](https://cloud.google.com/kms/) - Googles Key Management Service

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/treatspace/encrypter/tags). 

## Authors

* **Mike Schroeder** - *Initial work* - [_mschroeder](https://gitlab.com/_mschroeder)

See also the list of [contributors](https://gitlab.com/treatspace/encrypter/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/treatspace/encrypter/blob/master/LICENSE) file for details
